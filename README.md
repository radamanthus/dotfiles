# Dotfiles

My collection of dotfiles for easily clonging machine and project configurations.

- [✓] Fish shell config files
- [✓] Jekyll config files
  - [✓] config file
  - [✓] .gitignore
  - [✓] .gitlab-ci.yml
- [✓] Kitty Terminal
- [ ] Rails
  - [ ] .gitignore
  - [ ] Rubocop config file
- [✓] Vim

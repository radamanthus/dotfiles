# Temporary - Learning stuff
alias quotes "cd /home/rad/projects/learning/quote-editor"

alias easy "cd /home/rad/projects/easyship-challenges-master"

# My workflow
alias blog "cd /home/rad/projects/blog"
alias mfx "cd /home/rad/projects/MovieFX"
alias n "vi /home/rad/notes.txt"
alias ssa "ssh-add /home/rad/.ssh/id_ed25519"
alias vim-diary "cd /home/rad/projects/vim-diary"

# Buddyhub
alias buddy "cd /home/rad/projects/buddyhub"

# MegaEvents/trip-cart
alias tc "cd /home/rad/projects/megaevents/trip-cart"
alias tcrun "tc; nvm use 18.6.0; be rails s"

# Learning projects
alias buildalib "cd /home/rad/projects/learning/buildalib"

# Bundler
alias be "bundle exec"
alias bec "bundle exec rubocop"
alias ber "bundle exec rspec"

# Foreman
alias befrs "bundle exec foreman run sidekiq"
alias befrw "bundle exec foreman run web"
alias frs "foreman run sidekiq"
alias frw "foreman run web"

# Git
alias gbd "git branch -D"
alias gbl "git branch -l"
alias gc "git checkout"
alias gcp "git cherry-pick"
alias gd "git diff"
alias gl "git log --graph --decorate --pretty=oneline --abbrev-commit --all"
alias gpr "git pull --rebase origin"

alias gita "git add"
alias gits "git status"

# JS/Yarn
alias yd "yarn dev"
alias ys "yarn start"

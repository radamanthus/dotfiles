#!/usr/bin/fish

# Clear all keys
g810-led -a 000000

# Numeric keys at the top row
g810-led -kn 1 a0efa0
g810-led -kn 2 a0efa0
g810-led -kn 3 a0efa0
g810-led -kn 4 a0efa0
g810-led -kn 5 a0efa0
g810-led -kn 6 a0efa0
g810-led -kn 7 a0efa0
g810-led -kn 8 a0efa0
g810-led -kn 9 a0efa0
g810-led -kn 0 a0efa0
g810-led -k - a0efa0
g810-led -k = a0efa0

# F J space semicolon
g810-led -kn f a0efa0
g810-led -kn j a0efa0
g810-led -kn space a0efa0
g810-led -kn semicolon a0efa0

# Arrow keys
g810-led -kn arrow_top a0efa0
g810-led -kn arrow_bottom a0efa0
g810-led -kn arrow_left a0efa0
g810-led -kn arrow_right a0efa0

g810-led -c



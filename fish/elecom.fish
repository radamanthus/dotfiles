# TODO: Run `xinput list` to determine the elecom device ID instead of hard-coding it here
xinput set-button-map 17 1 2 2 4 5 6 7 8 2 1 11 3

# Set button Fn2 to scroll mode
xinput set-prop "pointer:ELECOM TrackBall Mouse HUGE TrackBall" 'libinput Button Scrolling Button' 1
xinput set-prop "pointer:ELECOM TrackBall Mouse HUGE TrackBall" 'libinput Scroll Method Enabled' 0 0 1

